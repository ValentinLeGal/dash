# Dash

Intégration d'une maquette d'un graphiste en site web (adaptée en responsive). Ce site présente l'application de gestion (fictive) Dash.

![Dash cover](./public/img/cover.png)

## À propos du projet

- **Nature :** projet personnel
- **Date :** février 2020
- **Description :** il s'agit d'une intégration d'une maquette d'un graphiste en site web. Ce site présente une application de gestion (fictive) nommée Dash. Le site web reste un site statique, non responsive (pour ne pas dénigrer le travail du graphiste)

## Technologies utilisées

- HTML
- CSS

## En savoir plus

- Lien vers la maquette originale : [Dribbble](https://dribbble.com/shots/6551060-Task-management-web-app-landing-page-Free-XD-Download)
- Lien vers le graphiste de cette maquette : [Mohammad Shohag](https://dribbble.com/shohag4y)